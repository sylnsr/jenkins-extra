# jenkins-extra
Jenkins with extras

## Extras:

### AWS CLI
Note that `aws configure` is not executed, which sets default region and
other setting. Please read the AWS CLI installation instructions and set
that manually by mounting the `.aws` directory on the host .. or some
other solution that works for you.

### Postgresql 10 client
For backing up Postgres and running some SQL with Ansible

### Ansible
For running Ansible playbooks
