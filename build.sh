#!/usr/bin/env bash
cd "$(dirname "$0")"
docker build --no-cache -t sylnsr/jenkins-extra:lts .

if [ $? -eq 0 ]; then
read -p "Push? (y/N)" -n 1 -r
echo    # (optional) move to a new line
	if [[ $REPLY =~ ^[Yy]$ ]]
	then
		docker push sylnsr/jenkins-extra:lts
	fi
fi