FROM jenkins/jenkins:lts
# image is based on Debian 9 (Stretch)

ENV DEBIAN_FRONTEND noninteractive

USER root

# http://docs.ansible.com/ansible/latest/intro_installation.html#latest-releases-via-apt-debian
RUN echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" >> /etc/apt/sources.list
RUN	apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367

# PG repo for the PG client tools
RUN echo 'deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main' >> /etc/apt/sources.list.d/pgdg.list
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

RUN apt-get update \
	&& apt-get install -y \
	lsb-core \
	lsb-release \
	ansible \
	apt-transport-https \
	sudo \
	python3 \
	python3-pip \
	postgresql-client-10 \
	nano \
	&& pip3 install awscli boto3 botocore

RUN rm -rf /var/lib/apt/lists/*

USER jenkins